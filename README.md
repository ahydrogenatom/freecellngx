# README #

NGX Technical Challenge: Freecell

Start Time: August 14, 4:00PM
End Time: August 15, 2:30PM

Game is functional. I was, however, unclear on what was meant by "Clicking on 'open'" meant in the context of the rules. I put in some code
that would make it easy to hook up whatever this functionality actually meant, and the GameManager has references to all free cell locations, 
and checking for empty locations is already a function. I'm curious as to whether this meant putting in buttons for each stack? "Open" wasn't
a term in the game's vocabulary from what I could find.

Other than that everything should be working. I've been in Unreal for quite a while now, so just getting used to how Unity works again took
a decent chunk of time. The JSON IO is programmed and available to look at, but wasn't hooked into the main game functionality. 

With more time, I would like to go back and fix the particles I added, as they did not appear to work when running the game. (A particle system should
activate when a card is placed onto a foundation correctly). Of course, more art/animations would be added as well. 

The lack of using Unity for a while caused some scaling issues as well, as trying to fiddle with those options was eating a lot of my time. On some
displays, card size seems fine, on others they end up very small. Again, this would be fixed with more research time or access to teammates with more
recent Unity expereince. 

Had a fun time! I'm bad at card games, even one's I've programmed :)

~Nick
